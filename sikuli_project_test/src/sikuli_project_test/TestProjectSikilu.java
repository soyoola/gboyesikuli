package sikuli_project_test;

import org.junit.Test;
import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

public class TestProjectSikilu {
	/**
	 * TEST CASE FOR VERIFYING VALID LOGIN
	 * PLEASE BEFORE RUNING THIS TEST SYSTEM REMEMBER TO CLEAN UP LOGIN WINDOW FROM USERNAME AND PASSWORD!
	 * AFTER CLEANING, USERNAME FIELD SHOULD BE SELCETED BEFORE CLOSING SPOTIFY WINDOW!!!
	 * please remember to change the path to the image file this is located in the
	 * image folder within the project. if this is not changed the application is going to fail!!
	 */
	@Test 
	public void TestSpotifyLogin() throws FindFailed{
		//lunch spotify application
		App spotify =  App.open ("C:\\Users\\Peter\\AppData\\Roaming\\Spotify\\spotify.exe");
		Screen screen = new Screen();
		Pattern dialogWindow = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\spotify_window_two.png");
		Pattern loginUsername = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\logindialog.png");
		Pattern loginUserpassword = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\password_field.png");
		Pattern loginBotton = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\clickLoginBotton.png");
		//password for spotify
		Region userpassword = screen.wait(dialogWindow);
		userpassword.type(loginUserpassword, "computer999"); 
		//click on login button 
		Region clickLogin = screen.wait(loginBotton);
		clickLogin.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		spotify.close();
		
	}
	/**
	 * TEST CASE FOR VERIFYING INVALID LOGIN
	 * please remember to change the path to the image file this is located in the
	 * image folder within the project. if this is not changed the application is going to fail!!
	 */
	
	@Test
	public void TestSpotifyInvalideLogin() throws FindFailed{
		App spotify =  App.open ("C:\\Users\\Peter\\AppData\\Roaming\\Spotify\\spotify.exe");
		Screen screen = new Screen();
		Pattern dialogWindow = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\spotify_window_two.png");
		Pattern loginUsername = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\logindialog.png");
		Pattern loginUsernameTwo = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\logindialogTwo.png");
		Pattern loginUserpassword = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\password_field.png");
		Pattern loginBotton = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\clickLoginBotton.png");
		//password for spotify
		Region userpassword = screen.wait(dialogWindow);
		userpassword.type(loginUserpassword, "kelvin");
		//Username for spotify
		Region userName = screen.wait(loginUsernameTwo);
		userName.type(loginUsernameTwo, "smithesrs");
		//click on login button
		Region clickLogin = screen.wait(loginBotton);
		clickLogin.click();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		spotify.close();
		
	}
	/**
	 * TEST CASE FOR VERIFYING SIMPLE SEARCH FOR ALBUM, ARTIST AND RANDOM WORDS
	 * please remember to change the path to the image file this is located in the
	 * image folder within the project. if this is not changed the application is going to fail!!
	 */
	@Test 
	public void TestSpotifySearch() throws FindFailed{
		App spotify =  App.open ("C:\\Users\\Peter\\AppData\\Roaming\\Spotify\\spotify.exe");
		Screen screen = new Screen();
		Pattern dialogWindow = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\spotify_window.png");
		Pattern loginUsername = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\logindialog.png");
		Pattern loginUserpassword = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\password_field.png");
		Pattern loginBotton = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\clickLoginBotton.png");
		Pattern searchField = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\searchField.png");
		Pattern cancleSearch = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\cancleSearch.png");
		//password for spotify
		Region userpassword = screen.wait(dialogWindow);
		userpassword.type(loginUserpassword, "computer999");
		//Username for spotify
		Region userName = screen.wait(loginUsername);
		userName.type(loginUsername, "doyinsol");
		//click on login button
		Region clickLogin = screen.wait(loginBotton);
		clickLogin.click();
		//Valid search scenario for album search
		Region search = screen.wait(searchField);
		search.type(searchField, "britney jean" + Key.ENTER);
		Region cancle = screen.wait(cancleSearch);
		cancle.click();
		//Invalid search scenario for random word
		Region searchOne = screen.wait(searchField);
		searchOne.type(searchField, "spinoff ice" + Key.ENTER);
		cancle.click();
		//Valid search scenario for artist
		Region searchTwo = screen.wait(searchField);
		searchTwo.type(searchField, "kanye west" + Key.ENTER);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		spotify.close();
		
	}
	/**
	 * TEST CASE FOR VERIFYING PLAYING A TRACK, PAUSE AND PLAY FUNCTION
	 * please remember to change the path to the image file this is located in the
	 * image folder within the project. if this is not changed the application is going to fail!!
	 */
	@Test 
	public void TestSpotifyPlaySong() throws FindFailed{
		App spotify =  App.open ("C:\\Users\\Peter\\AppData\\Roaming\\Spotify\\spotify.exe");
		Screen screen = new Screen();
		Pattern dialogWindow = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\spotify_window_two.png");
		Pattern loginUsername = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\logindialog.png");
		Pattern loginUserpassword = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\password_field.png");
		Pattern loginBotton = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\clickLoginBotton.png");
		Pattern searchField = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\searchField.png");
		Pattern songToPlay = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\songToplay.png");
		Pattern pauseSong = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\pauseIcon.png");
		Pattern playSong = new Pattern("C:\\Users\\Peter\\Desktop\\Ruby_eclipse\\sikuli_project_test\\images\\playIcon.png");
		//password for spotify
		Region userpassword = screen.wait(dialogWindow);
		userpassword.type(loginUserpassword, "computer999");
		//click on login button
		Region clickLogin = screen.wait(loginBotton);
		clickLogin.click();
		//Valid search scenario for artist
		Region searchTwo = screen.wait(searchField); 
		searchTwo.type(searchField, "kanye west" + Key.ENTER);
		Region selectedSong = screen.wait(songToPlay);//select song thats should played
		selectedSong.doubleClick(); //Start playing music
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//pause song to test pause function
		Region clickpause = screen.wait(pauseSong);
		clickpause.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//play song to test pause function
		Region clickplay = screen.wait(playSong);
		clickplay.click();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		spotify.close();
		
	}
}
